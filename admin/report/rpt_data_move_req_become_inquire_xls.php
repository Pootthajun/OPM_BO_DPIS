<?
	include("../../php_scripts/connect_database.php");
	include("../../php_scripts/calendar_data.php");
	include ("../php_scripts/function_share.php");

	require_once "../../Excel/class.writeexcel_workbook.inc.php";
	require_once "../../Excel/class.writeexcel_worksheet.inc.php";

	ini_set("max_execution_time", $max_execution_time);
	
	$db_dpis1 = new connect_dpis($dpisdb_host, $dpisdb_name, $dpisdb_user, $dpisdb_pwd);
	$db_dpis2 = new connect_dpis($dpisdb_host, $dpisdb_name, $dpisdb_user, $dpisdb_pwd);
	
	if ($PER_TYPE == 1)		{ 
			$arr_search_from[] = "PER_POSITION b";   	
			$field_position = "b.PL_CODE, b.PM_CODE, b.PT_CODE, b.POS_NO"; 	
			$group_by = "b.PL_CODE, b.PM_CODE, b.PT_CODE, b.POS_NO";
			$arr_search_condition[] = "(a.POS_ID=b.POS_ID) and (a.LEVEL_NO=g.LEVEL_NO)"; 
		}elseif ($PER_TYPE == 2)	{ 
			$arr_search_from[] = "PER_POS_EMP b";	 	
			$field_position = "b.PN_CODE as PL_CODE, b.POEM_NO as POS_NO"; 
			$group_by = "b.PN_CODE, b.POEM_NO"; 
			$arr_search_condition[] = "(a.POEM_ID=b.POEM_ID) and (a.LEVEL_NO=g.LEVEL_NO)"; 
		}elseif ($PER_TYPE == 3)	{ 
			$arr_search_from[] = "PER_POS_EMPSER b";	 	
			$field_position = "b.EP_CODE as PL_CODE, b.POEMS_NO as POS_NO";  
			$group_by = "b.EP_CODE, b.POEMS_NO";
			$arr_search_condition[] = "(a.POEMS_ID=b.POEMS_ID) and (a.LEVEL_NO=g.LEVEL_NO)"; 
		} elseif ($PER_TYPE == 4)	{ 
			$arr_search_from[] = "PER_POS_TEMP b";	 	
			$field_position = "b.TP_CODE as PL_CODE, b.POT_NO as POS_NO";  
			$group_by = "b.TP_CODE, b.POT_NO";
			$arr_search_condition[] = "(a.POT_ID=b.POT_ID) and (a.LEVEL_NO=g.LEVEL_NO)"; 
		} // end if
	if (trim($PER_TYPE)) 		 	$arr_search_condition[] = "(a.PER_TYPE=$PER_TYPE)";
	if (trim($PL_PN_CODE)) {
		if ($PER_TYPE == 1) 			$arr_search_condition[] = "( b.PL_CODE='$PL_PN_CODE' )";
		elseif ($PER_TYPE == 2) 	$arr_search_condition[] = "( b.PN_CODE='$PL_PN_CODE' )";	
		elseif ($PER_TYPE == 3) 	$arr_search_condition[] = "( b.EP_CODE='$PL_PN_CODE' )";	
		elseif ($PER_TYPE == 4) 	$arr_search_condition[] = "( b.TP_CODE='$PL_PN_CODE' )";	
	}
	if (trim($PM_CODE)) 		 	$arr_search_condition[] = "(b.PM_CODE=$PM_CODE)";

	if(trim($LEVEL_START)) $arr_search_condition[] = "(a.LEVEL_NO >= '$LEVEL_START')";
	if(trim($LEVEL_END)) $arr_search_condition[] = "(a.LEVEL_NO <= '$LEVEL_END')";

	if (trim($ORG_ID)){ 		
		if($select_org_structure==0) 	$arr_search_condition[] = "(b.ORG_ID=$ORG_ID)";
		elseif($select_org_structure==1) $arr_search_condition[] = "(a.ORG_ID=$ORG_ID)"; 
	}elseif($DEPARTMENT_ID){
		$arr_search_condition[] = "(a.DEPARTMENT_ID=$DEPARTMENT_ID)";
	}elseif($MINISTRY_ID){
		$cmd = " select ORG_ID from PER_ORG where ORG_ID_REF=$MINISTRY_ID ";
		$db_dpis->send_cmd($cmd);
		while($data = $db_dpis->get_array()) $arr_department_all[] = $data[ORG_ID];
		
		$arr_department = array_unique($arr_department_all);
		$arr_search_condition[] = "(a.DEPARTMENT_ID  in (". implode(",", $arr_department) ."))";
	}elseif(trim($PV_CODE)){
		$cmd = " select 	ORG_ID
						 from   	PER_ORG
						 where  	OL_CODE='03' and PV_CODE='$PV_CODE'
						 order by ORG_ID ";
		if($SESS_ORG_STRUCTURE==1) $cmd = str_replace("PER_ORG", "PER_ORG_ASS", $cmd);
		$db_dpis->send_cmd($cmd);
		while($data = $db_dpis->get_array()) $arr_org[] = $data[ORG_ID];
		if($select_org_structure==0) 	$arr_search_condition[] = "(b.ORG_ID in (". implode(",", $arr_org) ."))";
		else if($select_org_structure==1) $arr_search_condition[] = "(a.ORG_ID in (". implode(",", $arr_org) ."))";
	} // end if
	
	if ($CHECK_DATE) $tmp_date = save_date($CHECK_DATE);
	else $tmp_date = date("Y-m-d");
	$field_effectivedate = "";
	if ($search_year11 || $search_year12 || $search_year31 || $search_year32 || $search_year41 || $search_year42 || $search_year51 || $search_year52 ) {
		if ($search_year11 || $search_year12){
			$arr_search_from[] = "PER_POSITIONHIS c";	
			$field_effectivedate .= ", min(c.POH_EFFECTIVEDATE) as POSITION_EFFECTIVEDATE";
			$arr_search_condition[] = "(not (trim(c.POH_EFFECTIVEDATE) = '-'))";
			$arr_search_condition[] = "(a.PER_ID=c.PER_ID)";
			if($PER_TYPE == 1) 			$arr_search_condition[] = "(b.PL_CODE=c.PL_CODE)";
			elseif($PER_TYPE == 2) 	$arr_search_condition[] = "(b.PN_CODE=c.PN_CODE)";
			elseif($PER_TYPE == 3) 	$arr_search_condition[] = "(b.EP_CODE=c.EP_CODE)";
			elseif($PER_TYPE == 4) 	$arr_search_condition[] = "(b.TP_CODE=c.TP_CODE)";

			if($search_year11 == $search_year12) $search_year12 += 1;				
			if($search_year11){
				$effectivedate_max = date_adjust($tmp_date, "y", ($search_year11 * -1));
				if($DPISDB=="odbc") $arr_having_condition[] = "(LEFT(trim(min(c.POH_EFFECTIVEDATE)), 10) <= '$effectivedate_max')";
				elseif($DPISDB=="oci8") $arr_having_condition[] = "(SUBSTR(trim(min(c.POH_EFFECTIVEDATE)), 1, 10) <= '$effectivedate_max')";
				elseif($DPISDB=="mysql") $arr_having_condition[] = "(LEFT(trim(min(c.POH_EFFECTIVEDATE)), 10) <= '$effectivedate_max')";
			} // end if
			if($search_year12){
				$effectivedate_min = date_adjust($tmp_date, "y", ($search_year12 * -1));
				if($DPISDB=="odbc") $arr_having_condition[] = "(LEFT(trim(min(c.POH_EFFECTIVEDATE)), 10) >= '$effectivedate_min')";
				elseif($DPISDB=="oci8") $arr_having_condition[] = "(SUBSTR(trim(min(c.POH_EFFECTIVEDATE)), 1, 10) >= '$effectivedate_min')";
				elseif($DPISDB=="mysql") $arr_having_condition[] = "(LEFT(trim(min(c.POH_EFFECTIVEDATE)), 10) >= '$effectivedate_min')";
			} // end if
		} // end if

		if ($search_year31 || $search_year32){
			$arr_search_from[] = "PER_POSITIONHIS d";	
			$field_effectivedate .= ", min(d.POH_EFFECTIVEDATE) as LEVEL_EFFECTIVEDATE";
			$arr_search_condition[] = "(not (trim(d.POH_EFFECTIVEDATE) = '-'))";
			$arr_search_condition[] = "(a.PER_ID=d.PER_ID)";
			//$arr_search_condition[] = "(a.LEVEL_NO=g.LEVEL_NO)";

			if($search_year31 == $search_year32) $search_year32 += 1;				
			if($search_year31){
				$effectivedate_max = date_adjust($tmp_date, "y", ($search_year31 * -1));
				if($DPISDB=="odbc") $arr_having_condition[] = "(LEFT(trim(min(d.POH_EFFECTIVEDATE)), 10) <= '$effectivedate_max')";
				elseif($DPISDB=="oci8") $arr_having_condition[] = "(SUBSTR(trim(min(d.POH_EFFECTIVEDATE)), 1, 10) <= '$effectivedate_max')";
				elseif($DPISDB=="mysql") $arr_having_condition[] = "(LEFT(trim(min(d.POH_EFFECTIVEDATE)), 10) <= '$effectivedate_max')";
			} // end if
			if($search_year32){
				$effectivedate_min = date_adjust($tmp_date, "y", ($search_year32 * -1));
				if($DPISDB=="odbc") $arr_having_condition[] = "(LEFT(trim(min(d.POH_EFFECTIVEDATE)), 10) >= '$effectivedate_min')";
				elseif($DPISDB=="oci8") $arr_having_condition[] = "(SUBSTR(trim(min(d.POH_EFFECTIVEDATE)), 1, 10) >= '$effectivedate_min')";
				elseif($DPISDB=="mysql") $arr_having_condition[] = "(LEFT(trim(min(d.POH_EFFECTIVEDATE)), 10) >= '$effectivedate_min')";
			} // end if
		} // end if

		if ($search_year41 || $search_year42){
			$arr_search_from[] = "PER_POSITIONHIS e";	
			$field_effectivedate .= ", min(e.POH_EFFECTIVEDATE) as POSNO_EFFECTIVEDATE";
			$arr_search_condition[] = "(not (trim(e.POH_EFFECTIVEDATE) = '-'))";
			$arr_search_condition[] = "(a.PER_ID=e.PER_ID)";
			$arr_search_condition[] = "(b.POS_NO=e.POH_POS_NO)";

			if($search_year41 == $search_year42) $search_year42 += 1;				
			if($search_year41){
				$effectivedate_max = date_adjust($tmp_date, "y", ($search_year41 * -1));
				if($DPISDB=="odbc") $arr_having_condition[] = "(LEFT(trim(min(e.POH_EFFECTIVEDATE)), 10) <= '$effectivedate_max')";
				elseif($DPISDB=="oci8") $arr_having_condition[] = "(SUBSTR(trim(min(e.POH_EFFECTIVEDATE)), 1, 10) <= '$effectivedate_max')";
				elseif($DPISDB=="mysql") $arr_having_condition[] = "(LEFT(trim(min(e.POH_EFFECTIVEDATE)), 10) <= '$effectivedate_max')";
			} // end if
			if($search_year42){
				$effectivedate_min = date_adjust($tmp_date, "y", ($search_year42 * -1));
				if($DPISDB=="odbc") $arr_having_condition[] = "(LEFT(trim(min(e.POH_EFFECTIVEDATE)), 10) >= '$effectivedate_min')";
				elseif($DPISDB=="oci8") $arr_having_condition[] = "(SUBSTR(trim(min(e.POH_EFFECTIVEDATE)), 1, 10) >= '$effectivedate_min')";
				elseif($DPISDB=="mysql") $arr_having_condition[] = "(LEFT(trim(min(e.POH_EFFECTIVEDATE)), 10) >= '$effectivedate_min')";
			} // end if
		} // end if

		if ($search_year51 || $search_year52){
			$arr_search_from[] = "PER_POSITIONHIS f";	
			$field_effectivedate .= ", min(f.POH_EFFECTIVEDATE) as ORG_EFFECTIVEDATE";
			$arr_search_condition[] = "(not (trim(f.POH_EFFECTIVEDATE) = '-'))";
			$arr_search_condition[] = "(a.PER_ID=f.PER_ID)";
			$arr_search_condition[] = "(b.ORG_ID=f.ORG_ID_3 or h.ORG_NAME=f.POH_ORG3)";
			
			if($search_year51 == $search_year52) $search_year52 += 1;
			if($search_year51){
				$effectivedate_max = date_adjust($tmp_date, "y", ($search_year51 * -1));
				if($DPISDB=="odbc")	$arr_having_condition[] = "(LEFT(trim(min(f.POH_EFFECTIVEDATE)), 10) <= '$effectivedate_max')";
				elseif($DPISDB=="oci8") $arr_having_condition[] = "(SUBSTR(trim(min(f.POH_EFFECTIVEDATE)), 1, 10) <= '$effectivedate_max')";
				elseif($DPISDB=="mysql") $arr_having_condition[] = "(LEFT(trim(min(f.POH_EFFECTIVEDATE)), 10) <= '$effectivedate_max')";
			} // end if
			if($search_year52){
				$effectivedate_min = date_adjust($tmp_date, "y", ($search_year52 * -1));
				if($DPISDB=="odbc") $arr_having_condition[] = "(LEFT(trim(min(f.POH_EFFECTIVEDATE)), 10) >= '$effectivedate_min')";
				elseif($DPISDB=="oci8")	$arr_having_condition[] = "(SUBSTR(trim(min(f.POH_EFFECTIVEDATE)), 1, 10) >= '$effectivedate_min')";
				elseif($DPISDB=="mysql")	$arr_having_condition[] = "(LEFT(trim(min(f.POH_EFFECTIVEDATE)), 10) >= '$effectivedate_min')";
			} // end if
		} // end if
	}

	if ($search_year21 || $search_year22){
		if($search_year21 == $search_year22) $search_year22 += 1;
		if($search_year21){
			$startdate_max = date_adjust($tmp_date, "y", ($search_year21 * -1));
			if($DPISDB=="odbc") $arr_search_condition[] = "(LEFT(trim(a.PER_STARTDATE), 10) <= '$startdate_max')";
			elseif($DPISDB=="oci8") $arr_search_condition[] = "(SUBSTR(trim(a.PER_STARTDATE), 1, 10) <= '$startdate_max')";
			elseif($DPISDB=="mysql") $arr_search_condition[] = "(LEFT(trim(a.PER_STARTDATE), 10) <= '$startdate_max')";
		} // end if
		if($search_year22){
			$startdate_min = date_adjust($tmp_date, "y", ($search_year22 * -1));
			if($DPISDB=="odbc") $arr_search_condition[] = "(LEFT(trim(a.PER_STARTDATE), 10) >= '$startdate_min')";
			elseif($DPISDB=="oci8") $arr_search_condition[] = "(SUBSTR(trim(a.PER_STARTDATE), 1, 10) >= '$startdate_min')";
			elseif($DPISDB=="mysql") $arr_search_condition[] = "(LEFT(trim(a.PER_STARTDATE), 10) >= '$startdate_min')";
		} // end if
	} // end if

	$having_clause = $search_condition = $search_from = "";
	if(count($arr_search_from))			$search_from			= ", " . implode(", ", $arr_search_from);
	if(count($arr_search_condition)) 	$search_condition 	= implode(" and ", $arr_search_condition);	
	if(count($arr_having_condition)) 	$having_clause 		= " having " . implode(" and ", $arr_having_condition);

	$company_name = "";
	if ($BKK_FLAG==1 || $ISCS_FLAG==1)
		$report_title = "�ͺ��������Ţ���Ҫ���������Ѻ����¹";
	else
		$report_title = "�ͺ��������Ţ���Ҫ���/�١��ҧ������Ѻ����¹";
	$report_code = "P0303";
	
	$token = md5(uniqid(rand(), true)); 
	$fname= "../../Excel/tmp/dpis_$token.xls";

	$workbook = new writeexcel_workbook($fname);
	$worksheet = &$workbook->addworksheet("$report_code");
	$worksheet->set_margin_right(0.50);
	$worksheet->set_margin_bottom(1.10);
	
	//====================== SET FORMAT ======================//	
	require_once "../../Excel/my.defined_format.inc.php";	// use set_format(set of parameter) funtion , help is in file
	//====================== SET FORMAT ======================//
	
	function print_header(){
		global $worksheet, $xlsRow, $MINISTRY_TITLE, $DEPARTMENT_TITLE, $ORG_TITLE, $ORG_TITLE1, $ORG_TITLE2;

		$worksheet->set_column(0, 0, 10);
		$worksheet->set_column(1, 1, 30);
		$worksheet->set_column(2, 2, 40);
		$worksheet->set_column(3, 3, 20);
		$worksheet->set_column(4, 4, 25);
		$worksheet->set_column(5, 5, 30);
		$worksheet->set_column(6, 6, 20);
		$worksheet->set_column(7, 7, 20);
		$worksheet->set_column(8, 8, 20);
		$worksheet->set_column(9, 9, 20);
		$worksheet->set_column(10, 10, 20);
		$worksheet->set_column(11,11, 20);
		$worksheet->set_column(12, 12, 20);

		$xlsRow++;
		$worksheet->write($xlsRow, 0, "�ӴѺ���", set_format("xlsFmtTableHeader", "B", "C", "TLRB", 0));
		$worksheet->write($xlsRow, 1, "����-ʡ��", set_format("xlsFmtTableHeader", "B", "C", "TLRB", 0));
		$worksheet->write($xlsRow, 2, "���˹�/�дѺ", set_format("xlsFmtTableHeader", "B", "C", "TLRB", 0));
		$worksheet->write($xlsRow, 3, "$MINISTRY_TITLE", set_format("xlsFmtTableHeader", "B", "C", "TLRB", 0));
		$worksheet->write($xlsRow, 4, "$DEPARTMENT_TITLE", set_format("xlsFmtTableHeader", "B", "C", "TLRB", 0));
		$worksheet->write($xlsRow, 5, "$ORG_TITLE", set_format("xlsFmtTableHeader", "B", "C", "TLRB", 0));
		$worksheet->write($xlsRow, 6, "$ORG_TITLE1", set_format("xlsFmtTableHeader", "B", "C", "TLRB", 0));
		$worksheet->write($xlsRow, 7, "$ORG_TITLE2", set_format("xlsFmtTableHeader", "B", "C", "TLRB", 0));
		$worksheet->write($xlsRow, 8, "�������ҷ���ç���˹�", set_format("xlsFmtTableHeader", "B", "C", "TLRB", 0));
		$worksheet->write($xlsRow, 9, "�����Ҫ���(��)", set_format("xlsFmtTableHeader", "B", "C", "TLRB", 0));
		$worksheet->write($xlsRow, 10, "�������ҷ��������дѺ", set_format("xlsFmtTableHeader", "B", "C", "TLRB", 0));
		$worksheet->write($xlsRow, 11, "�������ҷ��������Ţ�����˹�", set_format("xlsFmtTableHeader", "B", "C", "TLRB", 0));
		$worksheet->write($xlsRow, 12, "�������ҷ�������$ORG_TITLE", set_format("xlsFmtTableHeader", "B", "C", "TLRB", 0));
	} // function		
		
	if($order_by==1){	//����-ʡ��
		$order_str = "ORDER BY PER_NAME $SortType[$order_by], PER_SURNAME $SortType[$order_by]";
  	}elseif($order_by==2) {	//���˹�
		$order_str = "ORDER BY a.LEVEL_NO  ".$SortType[$order_by];
  	} elseif($order_by==3) {	//�ѧ�Ѵ
		$order_str = "ORDER BY b.ORG_ID ".$SortType[$order_by];
	}

	if($DPISDB=="odbc"){
		$cmd = " select	a.PER_ID, $field_position, a.PN_CODE, PER_NAME, PER_SURNAME, a.LEVEL_NO, a.PER_STARTDATE, b.ORG_ID, 
										g.LEVEL_NAME , g.POSITION_LEVEL, a.DEPARTMENT_ID
										$field_effectivedate
						from 		PER_PERSONAL a, PER_LEVEL g	, PER_ORG h	
										$search_from 
						where	PER_STATUS=1 and b.ORG_ID=h.ORG_ID 
										$search_condition 
						group by a.PER_ID, $group_by, a.PN_CODE, PER_NAME, PER_SURNAME, a.LEVEL_NO, g.LEVEL_NAME, g.POSITION_LEVEL, 
										a.PER_STARTDATE, b.ORG_ID, a.DEPARTMENT_ID 
										$having_clause
						$order_str ";	
	}elseif($DPISDB=="oci8"){
		$cmd = " select 	distinct a.PER_ID, $field_position, a.PN_CODE, PER_NAME, PER_SURNAME, a.LEVEL_NO, a.PER_STARTDATE, b.ORG_ID, 
										g.LEVEL_NAME, g.POSITION_LEVEL, a.DEPARTMENT_ID
										$field_effectivedate
						  from 		PER_PERSONAL a, PER_LEVEL g, PER_ORG h
										$search_from 
						  where	PER_STATUS=1 and b.ORG_ID=h.ORG_ID and 
										$search_condition 
						  group by  a.PER_ID, $group_by, a.PN_CODE, PER_NAME, PER_SURNAME, a.LEVEL_NO, g.POSITION_LEVEL, g.LEVEL_NAME, 
										a.PER_STARTDATE, b.ORG_ID, a.DEPARTMENT_ID  
										$having_clause
						  $order_str ";
	}elseif($DPISDB=="mysql"){
		$cmd = " select	a.PER_ID, $field_position, a.PN_CODE, PER_NAME, PER_SURNAME, a.LEVEL_NO, a.PER_STARTDATE, b.ORG_ID, 
										g.LEVEL_NAME, g.POSITION_LEVEL, a.DEPARTMENT_ID
										$field_effectivedate
						from 		PER_PERSONAL a, PER_LEVEL g, PER_ORG h
										$search_from 
						where	PER_STATUS=1 and b.ORG_ID=h.ORG_ID and 
										$search_condition 
						group by a.PER_ID, $group_by, a.PN_CODE, PER_NAME, PER_SURNAME, a.LEVEL_NO, g.LEVEL_NAME, g.POSITION_LEVEL, 
										a.PER_STARTDATE, b.ORG_ID, a.DEPARTMENT_ID 
										$having_clause
						$order_str ";	
	} // end if
	$count_data = $db_dpis->send_cmd($cmd);
//	$db_dpis->show_error();
//echo $cmd;
	$data_count = $data_row = 0;
	while($data = $db_dpis->get_array()){
		$show_data = 0;

		$TMP_PER_ID = $data[PER_ID];
		$POS_NO = trim($data[POS_NO]);
		$ORG_ID = trim($data[ORG_ID]);
		if ($ORG_ID) {
			$cmd = " select ORG_NAME from PER_ORG where ORG_ID=$ORG_ID ";
			$db_dpis2->send_cmd($cmd);
			$data2 = $db_dpis2->get_array();
			$TMP_ORG_NAME = $data2[ORG_NAME];
		}

		$TMP_ORG_TIME = $TMP_POH_ORGDATE = "";
		$cmd = " select			POH_EFFECTIVEDATE, POH_ORG3
						 from			PER_POSITIONHIS
						 where			PER_ID=$TMP_PER_ID
						 order by		POH_EFFECTIVEDATE desc ";
		$db_dpis1->send_cmd($cmd);
//		echo "$cmd<br>";
//		$db_dpis1->show_error();
		 while ($data1 = $db_dpis1->get_array()) {
			$POH_ORG3 = trim($data1[POH_ORG3]);
			if ($POH_ORG3==$TMP_ORG_NAME) 
				$TMP_POH_ORGDATE = substr(trim($data1[POH_EFFECTIVEDATE]), 0, 10);
			else break;
		}
		if(trim($TMP_POH_ORGDATE) == "-") $TMP_POH_ORGDATE = "";
		if($TMP_POH_ORGDATE) {
			$TMP_ORG_TIME = date_difference($tmp_date, $TMP_POH_ORGDATE, "full");			
			$TMP_ORG_YEAR = date_difference($tmp_date, $TMP_POH_ORGDATE, "y");
			$TMP_POH_ORGDATE = show_date_format($TMP_POH_ORGDATE, 1);
		}
		if ($search_year51 && $search_year52) {
			if (($search_year51 && $TMP_ORG_YEAR >= $search_year51) && ($search_year52 && $TMP_ORG_YEAR <= $search_year52)) $show_data = 1;
		} elseif ($search_year51 || $search_year52) {
			if (($search_year51 && $TMP_ORG_YEAR >= $search_year51) || ($search_year52 && $TMP_ORG_YEAR <= $search_year52)) $show_data = 1;
		} else {
			$show_data = 1;
		}

		if ($show_data==1) { 
		$data_row++;
		$TMP_DEPARTMENT_ID = trim($data[DEPARTMENT_ID]);
		$cmd = " select ORG_NAME from PER_ORG where ORG_ID=$TMP_DEPARTMENT_ID ";
		$db_dpis1->send_cmd($cmd);
		$data1 = $db_dpis1->get_array();
		$TMP_DEPARTMENT_NAME = $data1[ORG_NAME];

		$PN_CODE = trim($data[PN_CODE]);
		if ($PN_CODE) {
			$cmd = " select PN_NAME from PER_PRENAME where PN_CODE='$PN_CODE' ";
			$db_dpis2->send_cmd($cmd);
			$data2 = $db_dpis2->get_array();
			$TMP_PN_NAME = $data2[PN_NAME];
		}
 		$TMP_PER_NAME = $TMP_PN_NAME . trim($data[PER_NAME]) . " " . trim($data[PER_SURNAME]);
		$LEVEL_NO = trim($data[LEVEL_NO]);
		$LEVEL_NAME = trim($data[LEVEL_NAME]);
		$POSITION_LEVEL = trim($data[POSITION_LEVEL]);
		if ($PER_TYPE == 1) {
			$PL_CODE = trim($data[PL_CODE]);
			$PT_CODE = trim($data[PT_CODE]);
			
			$cmd = "	select PL_NAME from PER_LINE where trim(PL_CODE)='$PL_CODE' ";
			$db_dpis2->send_cmd($cmd);
//			$db_dpis2->show_error();
			$data2 = $db_dpis2->get_array();
			$PL_NAME = trim($data2[PL_NAME]);

			$cmd = "	select PT_NAME from PER_TYPE where trim(PT_CODE)='$PT_CODE' ";
			$db_dpis2->send_cmd($cmd);
//			$db_dpis2->show_error();
			$data2 = $db_dpis2->get_array();
			$PT_NAME = trim($data2[PT_NAME]);
			$POSITION_NAME = trim($PL_NAME)?($PL_NAME . $POSITION_LEVEL . (($PT_NAME != "�����" && $LEVEL_NO >= 6)?"$PT_NAME":"")):" $LEVEL_NAME";
		} elseif ($PER_TYPE == 2) {
			$PN_CODE = trim($data[PL_CODE]);

			$cmd = "	select PN_NAME from PER_POS_NAME where PN_CODE='$PN_CODE' ";
			$db_dpis2->send_cmd($cmd);
//			$db_dpis2->show_error();
			$data2 = $db_dpis2->get_array();
			$POSITION_NAME = trim($data2[PN_NAME]).$LEVEL_NAME;
		} elseif ($PER_TYPE == 3) {
			$EP_CODE = trim($data[PL_CODE]);

			$cmd = "	select EP_NAME from PER_EMPSER_POS_NAME where EP_CODE='$EP_CODE' ";
			$db_dpis2->send_cmd($cmd);
//			$db_dpis2->show_error();
			$data2 = $db_dpis2->get_array();
			$POSITION_NAME = trim($data2[EP_NAME]).$LEVEL_NAME;
		}elseif ($PER_TYPE == 4) {
			$TP_CODE = trim($data[PL_CODE]);

			$cmd = "	select TP_NAME from PER_TEMP_POS_NAME where TP_CODE='$TP_CODE' ";
			$db_dpis2->send_cmd($cmd);
//			$db_dpis2->show_error();
			$data2 = $db_dpis2->get_array();
			$POSITION_NAME = trim($data2[TP_NAME])." ".$LEVEL_NAME;
		}
		$TMP_PER_STARTDATE = substr($data[PER_STARTDATE], 0, 10);
		$TMP_OFFICER_TIME = date_difference($tmp_date, $TMP_PER_STARTDATE, "full");
		$TMP_PER_STARTDATE = show_date_format($TMP_PER_STARTDATE, 1);
		
		$ORG_ID = $data[ORG_ID];
		$cmd = " select ORG_NAME from PER_ORG where ORG_ID=$ORG_ID ";
		if($SESS_ORG_STRUCTURE==1) $cmd = str_replace("PER_ORG", "PER_ORG_ASS", $cmd);
		$db_dpis2->send_cmd($cmd);
		$data2 = $db_dpis2->get_array();
		$ORG_NAME = $data2[ORG_NAME];
		//-----------------
	
		$ORG_ID_1 = $data[ORG_ID_1];
		$cmd = " select ORG_NAME from PER_ORG where ORG_ID=$ORG_ID_1 ";
		if($SESS_ORG_STRUCTURE==1) $cmd = str_replace("PER_ORG", "PER_ORG_ASS", $cmd);
		$db_dpis2->send_cmd($cmd);
		//$db_dpis2->show_error();
		$data2 = $db_dpis2->get_array();
		$ORG_1_NAME = $data2[ORG_NAME];
		
	//echo "ORG_ID_2 --  $data[ORG_ID_2]<br>";
		$ORG_ID_2 = $data[ORG_ID_2];
		
		$cmd = " select ORG_NAME from PER_ORG where ORG_ID=$ORG_ID_2";
		if($SESS_ORG_STRUCTURE==1) $cmd = str_replace("PER_ORG", "PER_ORG_ASS", $cmd);
		$db_dpis2->send_cmd($cmd);
		//$db_dpis2->show_error();
		$data2 = $db_dpis2->get_array();
		$ORG_2_NAME = $data2[ORG_NAME];
				
		$cmd = "select ORG_ID_REF from PER_ORG where ORG_ID= $ORG_ID";
		if($SESS_ORG_STRUCTURE==1) $cmd = str_replace("PER_ORG", "PER_ORG_ASS", $cmd);
		$db_dpis2->send_cmd($cmd);
		$data2 = $db_dpis2->get_array();
		$department = $data2[ORG_ID_REF];
		
		$cmd = "select ORG_NAME from PER_ORG where org_id = $department ";
		if($SESS_ORG_STRUCTURE==1) $cmd = str_replace("PER_ORG", "PER_ORG_ASS", $cmd);
		$db_dpis2->send_cmd($cmd);
		$data2 = $db_dpis2->get_array();				
		$DEPARTMENT_NAME = $data2[ORG_NAME];
		//----------------
		
		$cmd = "select ORG_ID_REF from PER_ORG where ORG_ID= $department";
		if($SESS_ORG_STRUCTURE==1) $cmd = str_replace("PER_ORG", "PER_ORG_ASS", $cmd);
		$db_dpis2->send_cmd($cmd);
		$data2 = $db_dpis2->get_array();
		$ministry = $data2[ORG_ID_REF];
		
		$cmd = "select ORG_NAME from PER_ORG where org_id = $ministry ";
		if($SESS_ORG_STRUCTURE==1) $cmd = str_replace("PER_ORG", "PER_ORG_ASS", $cmd);
		$db_dpis2->send_cmd($cmd);
		$data2 = $db_dpis2->get_array();				
		$MINISTRY_NAME  = $data2[ORG_NAME];
				
		if($PER_TYPE == 1){
			$cmd = " select			POH_EFFECTIVEDATE, PL_CODE
							 from			PER_POSITIONHIS 
							 where			PER_ID=$TMP_PER_ID 
							 order by		POH_EFFECTIVEDATE desc ";
		}elseif($PER_TYPE == 2){
			$cmd = " select			POH_EFFECTIVEDATE, PN_CODE
							 from			PER_POSITIONHIS 
							 where			PER_ID=$TMP_PER_ID 
							 order by		POH_EFFECTIVEDATE desc ";
		}elseif($PER_TYPE == 3){
			$cmd = " select			POH_EFFECTIVEDATE, EP_CODE
							 from			PER_POSITIONHIS 
							 where			PER_ID=$TMP_PER_ID 
							 order by		POH_EFFECTIVEDATE desc ";
		} elseif($PER_TYPE == 4){
			$cmd = " select			POH_EFFECTIVEDATE, TP_CODE
							 from			PER_POSITIONHIS 
							 where			PER_ID=$TMP_PER_ID 
							 order by		POH_EFFECTIVEDATE desc ";
		} // end if
		$db_dpis1->send_cmd($cmd);
//		$db_dpis1->show_error();
//		if ($TMP_PER_NAME=="�ҧ������� �������") echo $cmd;
                $TMP_POH_EFFECTIVEDATE="";
                $TMP_POSITION_TIME = "";
		 while ($data1 = $db_dpis1->get_array()) {
			if($PER_TYPE == 1) $TMP_PL_CODE = trim($data1[PL_CODE]);
			elseif($PER_TYPE == 2) $TMP_PN_CODE = trim($data1[PN_CODE]);
			elseif($PER_TYPE == 3) $TMP_EP_CODE = trim($data1[EP_CODE]);
			elseif($PER_TYPE == 4) $TMP_TP_CODE = trim($data1[TP_CODE]);
			if (($PER_TYPE == 1 && $TMP_PL_CODE==$PL_CODE) || ($PER_TYPE == 2 && $TMP_PN_CODE==$PN_CODE) || 
				($PER_TYPE == 3 && $TMP_EP_CODE==$EP_CODE) || ($PER_TYPE == 4 && $TMP_TP_CODE==$TP_CODE)) 
				$TMP_POH_EFFECTIVEDATE = substr(trim($data1[POH_EFFECTIVEDATE]), 0, 10);
			else break;
		}
		if(trim($TMP_POH_EFFECTIVEDATE) == "-") $TMP_POH_EFFECTIVEDATE = "";
		
		if($TMP_POH_EFFECTIVEDATE) {
			$TMP_POSITION_TIME = date_difference($tmp_date, $TMP_POH_EFFECTIVEDATE, "full");
			$TMP_POH_EFFECTIVEDATE = show_date_format($TMP_POH_EFFECTIVEDATE, 1);
		}
		
		$TMP_LEVEL_TIME = $TMP_POH_LEVELDATE = "";
		$cmd = " select			POH_EFFECTIVEDATE, LEVEL_NO
						 from			PER_POSITIONHIS
						 where			PER_ID=$TMP_PER_ID 
						 order by		POH_EFFECTIVEDATE desc ";
		$db_dpis1->send_cmd($cmd);
//		$db_dpis1->show_error();
		 while ($data1 = $db_dpis1->get_array()) {
			$POH_LEVEL_NO = trim($data1[LEVEL_NO]);
			if ($POH_LEVEL_NO==$LEVEL_NO) 
				$TMP_POH_LEVELDATE = substr(trim($data1[POH_EFFECTIVEDATE]), 0, 10);
			else break;
		}
		if(trim($TMP_POH_LEVELDATE) == "-") $TMP_POH_LEVELDATE = "";
		if($TMP_POH_LEVELDATE) {
			$TMP_LEVEL_TIME = date_difference($tmp_date, $TMP_POH_LEVELDATE, "full");
			$TMP_POH_LEVELDATE = show_date_format($TMP_POH_LEVELDATE, 1);
		}

		$TMP_POSNO_TIME = $TMP_POH_POSNODATE = "";
		$cmd = " select			POH_EFFECTIVEDATE, POH_POS_NO, POH_ORG2
						 from			PER_POSITIONHIS
						 where			PER_ID=$TMP_PER_ID 
						 order by		POH_EFFECTIVEDATE desc ";
		$db_dpis1->send_cmd($cmd);
//		echo "$cmd<br>";
//		$db_dpis1->show_error();
		 while ($data1 = $db_dpis1->get_array()) {
			$POH_POS_NO = trim($data1[POH_POS_NO]);
			$POH_ORG2 = trim($data1[POH_ORG2]);
			if ($POH_POS_NO==$POS_NO && $POH_ORG2==$TMP_DEPARTMENT_NAME) 
				$TMP_POH_POSNODATE = substr(trim($data1[POH_EFFECTIVEDATE]), 0, 10);
			else break;
		}
		if(trim($TMP_POH_POSNODATE) == "-") $TMP_POH_POSNODATE = "";
		if($TMP_POH_POSNODATE) {
			$TMP_POSNO_TIME = date_difference($tmp_date, $TMP_POH_POSNODATE, "full");
			$TMP_POH_POSNODATE = show_date_format($TMP_POH_POSNODATE, 1);
		}

		$arr_content[$data_count][type] = "CONTENT";
		$arr_content[$data_count][order] = $data_row;
		$arr_content[$data_count][per_name] = $TMP_PER_NAME;
		$arr_content[$data_count][position] = $POSITION_NAME;
		$arr_content[$data_count][position_time] = $TMP_POSITION_TIME."\n".$TMP_POH_EFFECTIVEDATE;
		$arr_content[$data_count][officer_time] = $TMP_OFFICER_TIME."\n".$TMP_PER_STARTDATE;
		$arr_content[$data_count][level_time] = $TMP_LEVEL_TIME."\n".$TMP_POH_LEVELDATE;
		$arr_content[$data_count][posno_time] = $TMP_POSNO_TIME."\n".$TMP_POH_POSNODATE;
		$arr_content[$data_count][org_time] = $TMP_ORG_TIME."\n".$TMP_POH_ORGDATE;
		$arr_content[$data_count][department_name] = $DEPARTMENT_NAME;
		$arr_content[$data_count][ministry_name] = $MINISTRY_NAME;
		$arr_content[$data_count][org_name] = $ORG_NAME;
		$arr_content[$data_count][org_1_name] = $ORG_1_NAME;
		$arr_content[$data_count][org_2_name] = $ORG_2_NAME;
				
		$data_count++;
		} // end if
	} // end while
	
//	echo "<pre>"; print_r($arr_content); echo "</pre>";
	
	if($count_data){
		$xlsRow = 0;
		$arr_title = explode("||", $report_title);
		for($i=0; $i<count($arr_title); $i++){
			$xlsRow = $i;
			$worksheet->write($xlsRow, 0, $arr_title[$i], set_format("xlsFmtTitle", "B", "C", "", 1));
			$worksheet->write($xlsRow, 1, "", set_format("xlsFmtTitle", "B", "C", "", 1));
			$worksheet->write($xlsRow, 2, "", set_format("xlsFmtTitle", "B", "C", "", 1));
			$worksheet->write($xlsRow, 3, "", set_format("xlsFmtTitle", "B", "C", "", 1));
			$worksheet->write($xlsRow, 4, "", set_format("xlsFmtTitle", "B", "C", "", 1));
			$worksheet->write($xlsRow, 5, "", set_format("xlsFmtTitle", "B", "C", "", 1));
			$worksheet->write($xlsRow, 6, "", set_format("xlsFmtTitle", "B", "C", "", 1));
			$worksheet->write($xlsRow, 7, "", set_format("xlsFmtTitle", "B", "C", "", 1));
			$worksheet->write($xlsRow, 8, "", set_format("xlsFmtTitle", "B", "C", "", 1));
			$worksheet->write($xlsRow, 9, "", set_format("xlsFmtTitle", "B", "C", "", 1));
			$worksheet->write($xlsRow, 10, "", set_format("xlsFmtTitle", "B", "C", "", 1));
			$worksheet->write($xlsRow, 11, "", set_format("xlsFmtTitle", "B", "C", "", 1));
			$worksheet->write($xlsRow, 12, "", set_format("xlsFmtTitle", "B", "C", "", 1));
		} // end if

		if($company_name){
			$xlsRow++;
			$worksheet->write($xlsRow, 0, $company_name, set_format("xlsFmtSubTitle", "B", "L", "", 1));
			$worksheet->write($xlsRow, 1, "", set_format("xlsFmtSubTitle", "B", "L", "", 1));
			$worksheet->write($xlsRow, 2, "", set_format("xlsFmtSubTitle", "B", "L", "", 1));
			$worksheet->write($xlsRow, 3, "", set_format("xlsFmtSubTitle", "B", "L", "", 1));
			$worksheet->write($xlsRow, 4, "", set_format("xlsFmtSubTitle", "B", "L", "", 1));
			$worksheet->write($xlsRow, 5, "", set_format("xlsFmtSubTitle", "B", "L", "", 1));
			$worksheet->write($xlsRow, 6, "", set_format("xlsFmtSubTitle", "B", "L", "", 1));
			$worksheet->write($xlsRow, 7, "", set_format("xlsFmtSubTitle", "B", "L", "", 1));
			$worksheet->write($xlsRow, 8, "", set_format("xlsFmtSubTitle", "B", "L", "", 1));
			$worksheet->write($xlsRow, 9, "", set_format("xlsFmtSubTitle", "B", "L", "", 1));
			$worksheet->write($xlsRow, 10, "", set_format("xlsFmtSubTitle", "B", "L", "", 1));
			$worksheet->write($xlsRow, 11, "", set_format("xlsFmtSubTitle", "B", "L", "", 1));
			$worksheet->write($xlsRow, 12, "", set_format("xlsFmtSubTitle", "B", "L", "", 1));
		} // end if

		print_header();

		for($data_count=0; $data_count<count($arr_content); $data_count++){
			$ORDER = $arr_content[$data_count][order];
			$PER_NAME = $arr_content[$data_count][per_name];
			$POSITION = $arr_content[$data_count][position];
			$POSITION_TIME = $arr_content[$data_count][position_time];
			$OFFICER_TIME = $arr_content[$data_count][officer_time];
			$LEVEL_TIME = $arr_content[$data_count][level_time];
			$POSNO_TIME = $arr_content[$data_count][posno_time];
			$ORG_TIME = $arr_content[$data_count][org_time];
			$DEPARTMENT_NAME = $arr_content[$data_count][department_name];
			$MINISTRY_NAME = $arr_content[$data_count][ministry_name];
			$ORG_NAME = $arr_content[$data_count][org_name];
			$ORG_1_NAME = $arr_content[$data_count][org_1_name];
			$ORG_2_NAME = $arr_content[$data_count][org_2_name];
			
			$xlsRow++;
			$worksheet->write_string($xlsRow, 0, (($NUMBER_DISPLAY==2)?convert2thaidigit($ORDER):$ORDER), set_format("xlsFmtTableDetail", "", "C", "TLRB", 0));
			$worksheet->write($xlsRow, 1, "$PER_NAME", set_format("xlsFmtTableDetail", "", "L", "TLRB", 0));
			$worksheet->write($xlsRow, 2, "$POSITION", set_format("xlsFmtTableDetail", "", "L", "TLRB", 0));
			$worksheet->write($xlsRow, 3, "$MINISTRY_NAME", set_format("xlsFmtTableDetail", "", "L", "TLRB", 0));
			$worksheet->write($xlsRow, 4, "$DEPARTMENT_NAME", set_format("xlsFmtTableDetail", "", "L", "TLRB", 0));
			$worksheet->write($xlsRow, 5, "$ORG_NAME", set_format("xlsFmtTableDetail", "", "L", "TLRB", 0));
			$worksheet->write($xlsRow, 6, "$ORG_1_NAME", set_format("xlsFmtTableDetail", "", "L", "TLRB", 0));
			$worksheet->write($xlsRow, 7, "$ORG_2_NAME", set_format("xlsFmtTableDetail", "", "L", "TLRB", 0));
			$worksheet->write($xlsRow, 8, (($NUMBER_DISPLAY==2)?convert2thaidigit($POSITION_TIME):$POSITION_TIME), set_format("xlsFmtTableDetail", "", "C", "TLRB", 0));
			$worksheet->write($xlsRow, 9,(($NUMBER_DISPLAY==2)?convert2thaidigit($OFFICER_TIME):$OFFICER_TIME), set_format("xlsFmtTableDetail", "", "C", "TLRB", 0));
			$worksheet->write($xlsRow, 10,(($NUMBER_DISPLAY==2)?convert2thaidigit($LEVEL_TIME):$LEVEL_TIME), set_format("xlsFmtTableDetail", "", "C", "TLRB", 0));
			$worksheet->write($xlsRow, 11, (($NUMBER_DISPLAY==2)?convert2thaidigit($POSNO_TIME):$POSNO_TIME), set_format("xlsFmtTableDetail", "", "C", "TLRB", 0));
			$worksheet->write($xlsRow, 12, (($NUMBER_DISPLAY==2)?convert2thaidigit($ORG_TIME):$ORG_TIME), set_format("xlsFmtTableDetail", "", "C", "TLRB", 0));
		} // end for				
	}else{
		$xlsRow = 0;
		$worksheet->write($xlsRow, 0, "***** ����բ����� *****", set_format("xlsFmtTitle", "B", "C", "", 1));
		$worksheet->write($xlsRow, 1, "", set_format("xlsFmtTitle", "B", "C", "", 1));
		$worksheet->write($xlsRow, 2, "", set_format("xlsFmtTitle", "B", "C", "", 1));
		$worksheet->write($xlsRow, 3, "", set_format("xlsFmtTitle", "B", "C", "", 1));
		$worksheet->write($xlsRow, 4, "", set_format("xlsFmtTitle", "B", "C", "", 1));
		$worksheet->write($xlsRow, 5, "", set_format("xlsFmtTitle", "B", "C", "", 1));
		$worksheet->write($xlsRow, 6, "", set_format("xlsFmtTitle", "B", "C", "", 1));
		$worksheet->write($xlsRow, 7, "", set_format("xlsFmtTitle", "B", "C", "", 1));
		$worksheet->write($xlsRow, 8, "", set_format("xlsFmtTitle", "B", "C", "", 1));
		$worksheet->write($xlsRow, 9, "", set_format("xlsFmtTitle", "B", "C", "", 1));
		$worksheet->write($xlsRow, 10, "", set_format("xlsFmtTitle", "B", "C", "", 1));
		$worksheet->write($xlsRow, 11, "", set_format("xlsFmtTitle", "B", "C", "", 1));
		$worksheet->write($xlsRow, 12, "", set_format("xlsFmtTitle", "B", "C", "", 1));
	} // end if

	$workbook->close();

	ini_set("max_execution_time", 30);
	
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
	header("Content-Type: application/x-msexcel; name=\"�ͺ��������Ţ���Ҫ���/�١��ҧ������Ѻ����¹.xls\"");
	header("Content-Disposition: inline; filename=\"�ͺ��������Ţ���Ҫ���/�١��ҧ������Ѻ����¹.xls\"");
	$fh=fopen($fname, "rb");
	fpassthru($fh);
	fclose($fh);
	unlink($fname);
?>