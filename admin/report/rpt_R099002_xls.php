<?
	include("../../php_scripts/connect_database.php");
	include("../../php_scripts/calendar_data.php");
	include ("../php_scripts/function_share.php");

	require_once "../../Excel/class.writeexcel_workbook.inc.php";
	require_once "../../Excel/class.writeexcel_worksheet.inc.php";
	
	ini_set("max_execution_time", $max_execution_time);

	$db_dpis2 = new connect_dpis($dpisdb_host, $dpisdb_name, $dpisdb_user, $dpisdb_pwd);

	if($_GET[NUMBER_DISPLAY])	$NUMBER_DISPLAY = $_GET[NUMBER_DISPLAY];
	$order_by = (isset($order_by))?  $order_by : 1;
  	if($order_by==1) $order_str = "a.KF_END_DATE, a.KF_CYCLE, PER_NAME, PER_SURNAME";
  	elseif($order_by==2) {
		if($DPISDB=="odbc") {
			if ($search_per_type==0 || $search_per_type==1) $order_str = "a.KF_END_DATE, a.KF_CYCLE, CLng(POS_NO)";
			elseif ($search_per_type==0 || $search_per_type==2) $order_str = "a.KF_END_DATE, a.KF_CYCLE, CLng(POEM_NO)";
			elseif ($search_per_type==0 || $search_per_type==3) $order_str = "a.KF_END_DATE, a.KF_CYCLE, CLng(POEMS_NO)";
			elseif ($search_per_type==0 || $search_per_type==4) $order_str = "a.KF_END_DATE, a.KF_CYCLE, CLng(POT_NO)";
		}elseif($DPISDB=="oci8"){
		 	if ($search_per_type==0 || $search_per_type==1) $order_str = "a.KF_END_DATE, a.KF_CYCLE, to_number(replace(POS_NO,'-',''))";
		 	elseif ($search_per_type==0 || $search_per_type==2) $order_str = "a.KF_END_DATE, a.KF_CYCLE, to_number(replace(POEM_NO,'-',''))";
		 	elseif ($search_per_type==0 || $search_per_type==3) $order_str = "a.KF_END_DATE, a.KF_CYCLE, to_number(replace(POEMS_NO,'-',''))";
		 	elseif ($search_per_type==0 || $search_per_type==4) $order_str = "a.KF_END_DATE, a.KF_CYCLE, to_number(replace(POT_NO,'-',''))";
		}elseif($DPISDB=="mysql"){ 
			if ($search_per_type==0 || $search_per_type==1) $order_str = "a.KF_END_DATE, a.KF_CYCLE, POS_NO+0";
			elseif ($search_per_type==0 || $search_per_type==2) $order_str = "a.KF_END_DATE, a.KF_CYCLE, POEM_NO+0";
			elseif ($search_per_type==0 || $search_per_type==3) $order_str = "a.KF_END_DATE, a.KF_CYCLE, POEMS_NO+0";
			elseif ($search_per_type==0 || $search_per_type==4) $order_str = "a.KF_END_DATE, a.KF_CYCLE, POT_NO+0";
		}
  	} elseif($order_by==3) $order_str = "a.KF_END_DATE, a.KF_CYCLE, f.LEVEL_SEQ_NO DESC, PER_NAME, PER_SURNAME";
  	elseif($order_by==4) {
		if ($search_per_type==0 || $search_per_type==1) $order_str = "a.KF_END_DATE, a.KF_CYCLE, c.ORG_ID, PER_NAME, PER_SURNAME";
		elseif ($search_per_type==0 || $search_per_type==2) $order_str = "a.KF_END_DATE, a.KF_CYCLE, d.ORG_ID, PER_NAME, PER_SURNAME";
		elseif ($search_per_type==0 || $search_per_type==3) $order_str = "a.KF_END_DATE, a.KF_CYCLE, e.ORG_ID, PER_NAME, PER_SURNAME";
		elseif ($search_per_type==0 || $search_per_type==4) $order_str = "a.KF_END_DATE, a.KF_CYCLE, g.ORG_ID, PER_NAME, PER_SURNAME";
	}

	if ($search_ministry_name) $MINISTRY_NAME = $search_ministry_name;
	if ($search_department_name) $DEPARTMENT_NAME = $search_department_name;
	if($select_org_structure==1){	//����Ѻ�ͺ���§ҹ
			if(trim($search_org_ass_id)){	$search_org_id=trim($search_org_ass_id);		}	
			if(trim($search_org_ass_id_1)){	$search_org_id_1=trim($search_org_ass_id_1);		}
			if(trim($search_org_ass_id_2)){	$search_org_id_2=trim($search_org_ass_id_2);		}
		}
	if(trim($search_org_id)){
		if($select_org_structure==0) $arr_search_condition[] = "(c.ORG_ID = $search_org_id)";
		else if($select_org_structure==1) $arr_search_condition[] = "(b.ORG_ID = $search_org_id)";
	}
	if(trim($search_org_id_1)){
	 	if($select_org_structure==0) $arr_search_condition[] = "(c.ORG_ID_1 = $search_org_id_1)";
		else if($select_org_structure==1) $arr_search_condition[] = "(b.ORG_ID_1 = $search_org_id_1)";
	}
	if(trim($search_org_id_2)){
	 	if($select_org_structure==0) $arr_search_condition[] = "(c.ORG_ID_2 = $search_org_id_2)";
		else if($select_org_structure==1) $arr_search_condition[] = "(b.ORG_ID_2 = $search_org_id_2)";
	}
	
	if($search_org_id1){
		$arr_search_condition[] = "(a.ORG_ID_KPI=$search_org_id1)";
  	}elseif($search_department_id){
		$arr_search_condition[] = "(a.DEPARTMENT_ID = $search_department_id)";
	}elseif($search_ministry_id){
		$cmd = " select ORG_ID from PER_ORG where ORG_ID_REF=$search_ministry_id ";
		$db_dpis->send_cmd($cmd);
		while($data = $db_dpis->get_array()) $arr_org[] = $data[ORG_ID];
		$arr_search_condition[] = "(a.DEPARTMENT_ID in (". implode(",", $arr_org) ."))";
	} // end if
	
  	if(trim($search_kf_year)){ 
		if($DPISDB=="odbc"){ 
			$arr_search_condition[] = "(LEFT(a.KF_START_DATE, 10) >= '". ($search_kf_year - 543 - 1)."-10-01')";
			$arr_search_condition[] = "(LEFT(a.KF_END_DATE, 10) < '". ($search_kf_year - 543)."-10-01')";
		}elseif($DPISDB=="oci8"){
			$arr_search_condition[] = "(SUBSTR(a.KF_START_DATE, 1, 10) >= '". ($search_kf_year - 543 - 1)."-10-01')";
			$arr_search_condition[] = "(SUBSTR(a.KF_END_DATE, 1, 10) < '". ($search_kf_year - 543)."-10-01')";
		}elseif($DPISDB=="mysql"){
			$arr_search_condition[] = "(LEFT(a.KF_START_DATE, 10) >= '". ($search_kf_year - 543 - 1)."-10-01')";
			$arr_search_condition[] = "(LEFT(a.KF_END_DATE, 10) < '". ($search_kf_year - 543)."-10-01')";
		} // end if
	} // end if
	if(trim($search_per_name)) $arr_search_condition[] = "(b.PER_NAME like '$search_per_name%')";
	if(trim($search_per_surname)) $arr_search_condition[] = "(b.PER_SURNAME like '$search_per_surname%')";
	$arr_search_condition[] = "(a.KF_CYCLE in (". implode(",", $search_kf_cycle) ."))";
	$search_condition = "";
	if(count($arr_search_condition)) $search_condition = " and " . implode(" and ", $arr_search_condition);

	if($DPISDB=="odbc"){
		$cmd = "	select a.KF_ID, a.KF_END_DATE, a.KF_CYCLE, b.PN_CODE, b.PER_NAME, b.PER_SURNAME, a.SUM_KPI, a.SUM_COMPETENCE, a.SUM_OTHER, a.PER_ID, a.PER_ID_REVIEW, a.PER_ID_REVIEW1, a.PER_ID_REVIEW2, CHIEF_PER_ID, FRIEND_FLAG, TOTAL_SCORE, a.ORG_ID_KPI, a.ORG_ID, b.LEVEL_NO, b.PER_TYPE, b.POS_ID, b.POEM_ID, b.POEMS_ID,b.POT_ID
							from 	(	
											(	
												(	
													(
													(
														PER_KPI_FORM a
														 	inner join PER_PERSONAL b on (a.PER_ID=b.PER_ID)
													)	left join PER_POSITION c on (b.POS_ID=c.POS_ID) 
												) 	left join PER_POS_EMP d on (b.POEM_ID=d.POEM_ID)
											) 	left join PER_POS_EMPSER e on (b.POEMS_ID=e.POEMS_ID)
										) 	left join PER_POS_TEMP g on (b.POT_ID=g.POT_ID)
										) 	left join PER_LEVEL f on (b.LEVEL_NO=f.LEVEL_NO)
											$search_condition
							order by 	$order_str ";
	}elseif($DPISDB=="oci8"){
		$min_rownum = (($current_page - 1) * $data_per_page) + 1;
		$max_rownum = $current_page * $data_per_page;

		$cmd = "select		a.KF_ID, a.KF_END_DATE, a.KF_CYCLE, b.PN_CODE, b.PER_NAME, b.PER_SURNAME, a.SUM_KPI, a.SUM_COMPETENCE, a.SUM_OTHER, a.PER_ID, a.PER_ID_REVIEW, a.PER_ID_REVIEW1, a.PER_ID_REVIEW2, CHIEF_PER_ID, FRIEND_FLAG, TOTAL_SCORE, a.ORG_ID_KPI, a.ORG_ID, b.LEVEL_NO, b.PER_TYPE, b.POS_ID, b.POEM_ID, b.POEMS_ID,b.POT_ID
								from		PER_KPI_FORM a, PER_PERSONAL b, PER_POSITION c, PER_POS_EMP d, PER_POS_EMPSER e, PER_LEVEL f ,PER_POS_TEMP g
								where		a.PER_ID=b.PER_ID and b.POS_ID=c.POS_ID(+) and b.POEM_ID=d.POEM_ID(+) and b.POEMS_ID=e.POEMS_ID(+) and b.POT_ID=g.POT_ID(+) and b.LEVEL_NO=f.LEVEL_NO(+)
												$search_condition
								order by 	$order_str ";
	}elseif($DPISDB=="mysql"){
		$cmd = "	select		a.KF_ID, a.KF_END_DATE, a.KF_CYCLE, b.PN_CODE, b.PER_NAME, b.PER_SURNAME, a.SUM_KPI, a.SUM_COMPETENCE, a.SUM_OTHER, a.PER_ID, a.PER_ID_REVIEW, a.PER_ID_REVIEW1, a.PER_ID_REVIEW2, CHIEF_PER_ID, FRIEND_FLAG, TOTAL_SCORE, a.ORG_ID_KPI, a.ORG_ID, b.LEVEL_NO, b.PER_TYPE, b.POS_ID, b.POEM_ID, b.POEMS_ID,b.POT_ID
							from 	(	
											(	
												(	
													(
													(
														PER_KPI_FORM a
														 	inner join PER_PERSONAL b on (a.PER_ID=b.PER_ID)
													)	left join PER_POSITION c on (b.POS_ID=c.POS_ID) 
												) 	left join PER_POS_EMP d on (b.POEM_ID=d.POEM_ID)
											) 	left join PER_POS_EMPSER e on (b.POEMS_ID=e.POEMS_ID)
											) 	left join PER_POS_TEMP g on (b.POT_ID=g.POT_ID)
										) 	left join PER_LEVEL f on (b.LEVEL_NO=f.LEVEL_NO)
											$search_condition
							order by 	$order_str";
	} // end if

	//echo $cmd;<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	$report_title = "��§ҹ��Ǫ���Ѵ��ºؤ��";

	if ($SESS_PROVINCE_NAME!=""){
		if ($SESS_PROVINCE_NAME=="��ا෾��ҹ��") $report_title .= "|" . $SESS_PROVINCE_NAME;
		else $report_title .= "|�ѧ��Ѵ" . $SESS_PROVINCE_NAME;
	}
	if ($SESS_USERGROUP_LEVEL <= 3) $report_title .= "|" . $MINISTRY_NAME;
	$report_title .= "|" . $DEPARTMENT_NAME;

	$report_code = "R9902";

	$token = md5(uniqid(rand(), true)); 
	$fname= "../../Excel/tmp/dpis_$token.xls";

	$workbook = new writeexcel_workbook($fname);
	$worksheet = &$workbook->addworksheet("");
	$worksheet->set_margin_right(0.50);
	$worksheet->set_margin_bottom(1.10);
	
	//====================== SET FORMAT ======================//
	require_once "../../Excel/my.defined_format.inc.php";	// use set_format(set of parameter) funtion , help is in file
	//====================== SET FORMAT ======================//

	function print_header($xlsRow,$CTRL_TYPE,$SESS_USERGROUP_LEVEL,$search_department_name,$search_ministry_name){
		global $worksheet, $ORG_TITLE, $DEPARTMENT_TITLE, $MINISTRY_TITLE;

		$worksheet->set_column(0, 0, 10);
		$worksheet->set_column(1, 1, 5);
		$worksheet->set_column(2, 2, 35);

		$worksheet->write($xlsRow, 0, "�է�����ҳ", set_format("xlsFmtTableHeader", "B", "C", "TBLR", 0));
		$worksheet->write($xlsRow, 1, "���駷��", set_format("xlsFmtTableHeader", "B", "C", "TBLR", 0));
		$worksheet->write($xlsRow, 2, "���ͼ���Ѻ��û����Թ", set_format("xlsFmtTableHeader", "B", "C", "TBLR", 0));

		if($CTRL_TYPE <= 3 && $SESS_USERGROUP_LEVEL <= 3 && !($search_department_name) ){
			if(!($search_ministry_name) ){
				$worksheet->set_column(3, 3, 50);
				$worksheet->set_column(4, 4, 50);
				$worksheet->set_column(5, 5, 50); 
				$worksheet->set_column(6, 6, 50); 
				$worksheet->set_column(7, 7, 10);
				$worksheet->set_column(8, 8, 10);
				$worksheet->set_column(9, 9, 10);
				$worksheet->set_column(10, 10, 12);
				$worksheet->write($xlsRow, 3, "���˹�", set_format("xlsFmtTableHeader", "B", "C", "TBLR", 0));
				$worksheet->write($xlsRow, 4, "$ORG_TITLE", set_format("xlsFmtTableHeader", "B", "C", "TBLR", 0));
				$worksheet->write($xlsRow, 5, "$DEPARTMENT_TITLE", set_format("xlsFmtTableHeader", "B", "C", "TBLR", 0));
				$worksheet->write($xlsRow, 6, "$MINISTRY_TITLE", set_format("xlsFmtTableHeader", "B", "C", "TBLR", 0));
				$worksheet->write($xlsRow, 7, "�����ķ���", set_format("xlsFmtTableHeader", "B", "C", "TBLR", 0));
				$worksheet->write($xlsRow, 8, "���ö��", set_format("xlsFmtTableHeader", "B", "C", "TBLR", 0));
				$worksheet->write($xlsRow, 9, "��� �", set_format("xlsFmtTableHeader", "B", "C", "TBLR", 0));
				$worksheet->write($xlsRow, 10, "�š�û����Թ", set_format("xlsFmtTableHeader", "B", "C", "TBLR", 0));
			}else{
				$worksheet->set_column(3, 3, 50);
				$worksheet->set_column(4, 4, 50);
				$worksheet->set_column(5, 5, 50); 
				$worksheet->set_column(6, 6, 10);
				$worksheet->set_column(7, 7, 10);
				$worksheet->set_column(8, 8, 10);
				$worksheet->set_column(9, 9, 12);
				$worksheet->write($xlsRow, 3, "���˹�", set_format("xlsFmtTableHeader", "B", "C", "TBLR", 0));
				$worksheet->write($xlsRow, 4, "$ORG_TITLE", set_format("xlsFmtTableHeader", "B", "C", "TBLR", 0));
				$worksheet->write($xlsRow, 5, "$DEPARTMENT_TITLE", set_format("xlsFmtTableHeader", "B", "C", "TBLR", 0));
				$worksheet->write($xlsRow, 6, "�����ķ���", set_format("xlsFmtTableHeader", "B", "C", "TBLR", 0));
				$worksheet->write($xlsRow, 7, "���ö��", set_format("xlsFmtTableHeader", "B", "C", "TBLR", 0));
				$worksheet->write($xlsRow, 8, "��� �", set_format("xlsFmtTableHeader", "B", "C", "TBLR", 0));
				$worksheet->write($xlsRow, 9, "�š�û����Թ", set_format("xlsFmtTableHeader", "B", "C", "TBLR", 0));
			}
		}else{
			$worksheet->set_column(3,3, 50);
			$worksheet->set_column(4, 4, 50);
			$worksheet->set_column(5, 5, 10);
			$worksheet->set_column(6, 6, 10);
			$worksheet->set_column(7, 7, 10);
			$worksheet->set_column(8, 8, 12);
			$worksheet->write($xlsRow, 3, "���˹�", set_format("xlsFmtTableHeader", "B", "C", "TBLR", 0));
			$worksheet->write($xlsRow, 4, "$ORG_TITLE", set_format("xlsFmtTableHeader", "B", "C", "TBLR", 0));
			$worksheet->write($xlsRow, 5, "�����ķ���", set_format("xlsFmtTableHeader", "B", "C", "TBLR", 0));
			$worksheet->write($xlsRow, 6, "���ö��", set_format("xlsFmtTableHeader", "B", "C", "TBLR", 0));
			$worksheet->write($xlsRow, 7, "��� �", set_format("xlsFmtTableHeader", "B", "C", "TBLR", 0));
			$worksheet->write($xlsRow, 8, "�š�û����Թ", set_format("xlsFmtTableHeader", "B", "C", "TBLR", 0));
		}
		
	} // end if

	$count_data = $db_dpis->send_cmd($cmd);
//$db_dpis->show_error();

	if($CTRL_TYPE <= 3 && $SESS_USERGROUP_LEVEL <= 3 && !($search_department_name) ){
		if(!($search_ministry_name) ){
			$iRound = 10;
		}else{
			$iRound = 9;
		}
	}else{
		$iRound = 8;
	}

	if($count_data){
		$xlsRow = 0;
		$arr_title = explode("|", $report_title);
		for($j=0; $j<count($arr_title); $j++){
			$xlsRow = $j;
			$worksheet->write($xlsRow, 0, $arr_title[$j], set_format("xlsFmtTitle", "B", "C", "", 1));
			for($i=1; $i<=$iRound; $i++) $worksheet->write($xlsRow, $i, "", set_format("xlsFmtTitle", "B", "C", "", 1));
		} // end if

		print_header($xlsRow+1,$CTRL_TYPE,$SESS_USERGROUP_LEVEL,$search_department_name,$search_ministry_name);
		$data_count = $xlsRow+1;
		
		while($data = $db_dpis->get_array()){
			$data_count++;
			$data_row++;			
			
			$TMP_KF_ID = $data[KF_ID];
			$current_list .= ((trim($current_list))?",":"") . $TMP_KF_ID;
			$KF_END_DATE = substr($data[KF_END_DATE], 0, 10);
			$KF_YEAR = substr($KF_END_DATE, 0, 4) + 543;
			$KF_END_DATE = show_date_format($KF_END_DATE,$DATE_DISPLAY);
			$KF_CYCLE = $data[KF_CYCLE];
			$PN_CODE = $data[PN_CODE];
			$SELF_PER_ID = $data[PER_ID];
			$CHIEF_PER_ID = $data[CHIEF_PER_ID]; 	// �������˹��
			$FRIEND_FLAG = $data[FRIEND_FLAG];
			$TOTAL_SCORE = $data[TOTAL_SCORE];
			$ORG_ID_KPI = $data[ORG_ID_KPI];
			$TMP_ORG_ID = $data[ORG_ID];

			$cmd = " select ORG_NAME from PER_ORG where ORG_ID = $TMP_ORG_ID ";
			$db_dpis2->send_cmd($cmd);
			$data2 = $db_dpis2->get_array();
			$TMP_ORG_NAME = trim($data2[ORG_NAME]);

			$PER_NAME = trim($data[PER_NAME]);
			$PER_SURNAME = trim($data[PER_SURNAME]);
			if ($TOTAL_SCORE)
				$RESULT = number_format($TOTAL_SCORE, 2);
			else
				$RESULT = number_format(($data[SUM_KPI] + $data[SUM_COMPETENCE]), 2);
			
			if ($data[SUM_KPI]!="") $SUM_KPI = number_format($data[SUM_KPI], 2);
			else $SUM_KPI = "-";

			if ($data[SUM_COMPETENCE]!="") $SUM_COMPETENCE = number_format($data[SUM_COMPETENCE], 2);
			else $SUM_COMPETENCE = "-";

			if ($data[SUM_OTHER]!="") $SUM_OTHER = number_format($data[SUM_OTHER], 2);
			else $SUM_OTHER = "-";

			$cmd = " select PN_NAME from PER_PRENAME where PN_CODE='$PN_CODE' ";
			$db_dpis2->send_cmd($cmd);
			$data2 = $db_dpis2->get_array();
			$PN_NAME = $data2[PN_NAME];
		
			$PER_FULLNAME = $PN_NAME . $PER_NAME . " " . $PER_SURNAME;
			
			$LEVEL_NO = trim($data[LEVEL_NO]);
			$PER_TYPE = $data[PER_TYPE];
			$POS_ID = $data[POS_ID];
			$POEM_ID = $data[POEM_ID];
			$POEMS_ID = $data[POEMS_ID];
			
			$cmd = " select LEVEL_NAME, POSITION_LEVEL from PER_LEVEL where LEVEL_NO='$LEVEL_NO' ";
			$db_dpis2->send_cmd($cmd);
			$data2 = $db_dpis2->get_array();
			$LEVEL_NAME = trim($data2[LEVEL_NAME]);
			$POSITION_LEVEL = $data2[POSITION_LEVEL];
			if (!$POSITION_LEVEL) $POSITION_LEVEL = $LEVEL_NAME;

			if($PER_TYPE == 1){
				$cmd = " select 	a.ORG_ID, d.ORG_NAME, b.PL_NAME, a.PT_CODE 
								from 		PER_POSITION a, PER_LINE b, PER_ORG d
								where 	a.POS_ID=$POS_ID and a.PL_CODE=b.PL_CODE and a.ORG_ID=d.ORG_ID ";
				$db_dpis2->send_cmd($cmd);
				$data2 = $db_dpis2->get_array();
				$TMP_PL_NAME = $data2[PL_NAME] . $POSITION_LEVEL;
			}elseif($PER_TYPE == 2){
				$cmd = " select	pl.PN_NAME, po.ORG_NAME    
								from	PER_POS_EMP pp, PER_POS_NAME pl, PER_ORG po 
								where	pp.POEM_ID=$POEM_ID and pp.ORG_ID=po.ORG_ID and pp.PN_CODE=pl.PN_CODE  ";
				$db_dpis2->send_cmd($cmd);
				$data2 = $db_dpis2->get_array();
				$PL_NAME = trim($data2[PN_NAME]);
				$TMP_PL_NAME = (trim($PL_NAME))? "$PL_NAME$POSITION_LEVEL" : "";	
			}elseif($PER_TYPE == 3){
				$cmd = " select	pl.EP_NAME, po.ORG_NAME   
								from	PER_POS_EMPSER pp, PER_EMPSER_POS_NAME pl, PER_ORG po 
								where	pp.POEMS_ID=$POEMS_ID and pp.ORG_ID=po.ORG_ID and pp.EP_CODE=pl.EP_CODE  ";
				$db_dpis2->send_cmd($cmd);
				$data2 = $db_dpis2->get_array();
				$PL_NAME = trim($data2[EP_NAME]);
				$TMP_PL_NAME = (trim($PL_NAME))? "$PL_NAME$POSITION_LEVEL" : "";	
			}elseif($PER_TYPE == 4){
				$cmd = " select	pl.TP_NAME, po.ORG_NAME   
								from	 PER_POS_TEMP pp, PER_TEMP_POS_NAME pl, PER_ORG po 
								where	pp.POT_ID=$POT_ID and pp.ORG_ID=po.ORG_ID and pp.TP_CODE=pl.TP_CODE  ";
				if($select_org_structure==1) $cmd = str_replace("PER_ORG", "PER_ORG_ASS", $cmd);
				$db_dpis2->send_cmd($cmd);
				$data2 = $db_dpis2->get_array();
				$PL_NAME = trim($data2[TP_NAME]);
				$TMP_PL_NAME = (trim($PL_NAME))? "$PL_NAME$POSITION_LEVEL" : "";	
			} // end if

			if($CTRL_TYPE <= 3 && $SESS_USERGROUP_LEVEL <= 3 && !($search_department_name) ){
				$cmd = " select ORG_ID_REF from PER_ORG where ORG_ID = $TMP_ORG_ID ";
				$db_dpis2->send_cmd($cmd);
				$data2 = $db_dpis2->get_array();
				$TMP_ORG_ID_REF1 = trim($data2[ORG_ID_REF]);

				$cmd = " select ORG_NAME,ORG_ID_REF from PER_ORG where ORG_ID = $TMP_ORG_ID_REF1 ";
				$db_dpis2->send_cmd($cmd);
				$data2 = $db_dpis2->get_array();
				$Department = trim($data2[ORG_NAME]);
				$TMP_ORG_ID_REF2 = trim($data2[ORG_ID_REF]);

				if(!($search_ministry_name) ){
					$cmd = " select ORG_NAME from PER_ORG where ORG_ID = $TMP_ORG_ID_REF2 ";
					$db_dpis2->send_cmd($cmd);
					$data2 = $db_dpis2->get_array();
					$Ministry = trim($data2[ORG_NAME]);
				}
			}

			$xlsRow = $data_count;
			$worksheet->write_string($xlsRow, 0, $KF_YEAR, set_format("xlsFmtTableDetail", "", "C", "TLRB", 0));
			$worksheet->write($xlsRow, 1, $KF_CYCLE, set_format("xlsFmtTableDetail", "", "C", "TLRB", 0));
			$worksheet->write($xlsRow, 2, $PER_FULLNAME, set_format("xlsFmtTableDetail", "", "L", "TLRB", 0));
			$worksheet->write($xlsRow, 3, $TMP_PL_NAME, set_format("xlsFmtTableDetail", "", "L", "TLRB", 0));
			if($CTRL_TYPE <= 3 && $SESS_USERGROUP_LEVEL <= 3 && !($search_department_name) ){
				if(!($search_ministry_name) ){
					$worksheet->write($xlsRow, 4, $TMP_ORG_NAME, set_format("xlsFmtTableDetail", "", "L", "TLRB", 0));
					$worksheet->write($xlsRow, 5, $Department, set_format("xlsFmtTableDetail", "", "L", "TLRB", 0));
					$worksheet->write($xlsRow, 6, $Ministry, set_format("xlsFmtTableDetail", "", "L", "TLRB", 0));
					$worksheet->write($xlsRow, 7, $SUM_KPI, set_format("xlsFmtTableDetail", "", "R", "TLRB", 0));
					$worksheet->write($xlsRow, 8, $SUM_COMPETENCE, set_format("xlsFmtTableDetail", "", "R", "TLRB", 0));
					$worksheet->write($xlsRow, 9, $SUM_OTHER, set_format("xlsFmtTableDetail", "", "R", "TLRB", 0));
					$worksheet->write($xlsRow, 10, $RESULT, set_format("xlsFmtTableDetail", "", "R", "TLRB", 0));
				}else{
					$worksheet->write($xlsRow, 4, $TMP_ORG_NAME, set_format("xlsFmtTableDetail", "", "L", "TLRB", 0));
					$worksheet->write($xlsRow, 5, $Department, set_format("xlsFmtTableDetail", "", "L", "TLRB", 0));
					$worksheet->write($xlsRow, 6, $SUM_KPI, set_format("xlsFmtTableDetail", "", "R", "TLRB", 0));
					$worksheet->write($xlsRow, 7, $SUM_COMPETENCE, set_format("xlsFmtTableDetail", "", "R", "TLRB", 0));
					$worksheet->write($xlsRow, 8, $SUM_OTHER, set_format("xlsFmtTableDetail", "", "R", "TLRB", 0));
					$worksheet->write($xlsRow, 9, $RESULT, set_format("xlsFmtTableDetail", "", "R", "TLRB", 0));
				}
			}else{
				$worksheet->write($xlsRow, 4, $TMP_ORG_NAME, set_format("xlsFmtTableDetail", "", "L", "TLRB", 0));
				$worksheet->write($xlsRow, 5, $SUM_KPI, set_format("xlsFmtTableDetail", "", "R", "TLRB", 0));
				$worksheet->write($xlsRow, 6, $SUM_COMPETENCE, set_format("xlsFmtTableDetail", "", "R", "TLRB", 0));
				$worksheet->write($xlsRow, 7, $SUM_OTHER, set_format("xlsFmtTableDetail", "", "R", "TLRB", 0));
				$worksheet->write($xlsRow, 8, $RESULT, set_format("xlsFmtTableDetail", "", "R", "TLRB", 0));
			}
		} // end while
	}else{
		$xlsRow = 0;
		$worksheet->write($xlsRow, 0, "***** ����բ����� *****", set_format("xlsFmtTitle", "B", "C", "", 1));
		for($i=1; $i<=$iRound; $i++) $worksheet->write($xlsRow, $i, "", set_format("xlsFmtTitle", "B", "C", "", 1));
	} // end if

	$workbook->close();

	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
	header("Content-Type: application/x-msexcel; name=\"$report_code.xls\"");
	header("Content-Disposition: inline; filename=\"$report_code.xls\"");
	$fh=fopen($fname, "rb");
	fpassthru($fh);
	fclose($fh);
	unlink($fname);
?>