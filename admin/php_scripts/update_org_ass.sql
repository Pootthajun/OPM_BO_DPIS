insert into per_org_ass (ORG_ID, ORG_CODE, ORG_NAME, ORG_SHORT, OL_CODE, OT_CODE,
							  ORG_ADDR1, ORG_ADDR2, ORG_ADDR3, CT_CODE, PV_CODE, AP_CODE, DT_CODE, ORG_DATE, ORG_JOB,
							  ORG_ID_REF, ORG_ACTIVE, ORG_SEQ_NO, ORG_WEBSITE, UPDATE_USER, UPDATE_DATE, 
							  ORG_ENG_NAME, POS_LAT, POS_LONG, ORG_DOPA_CODE, DEPARTMENT_ID, MG_CODE, PG_CODE)
select 3196, ORG_CODE, ORG_NAME, ORG_SHORT, OL_CODE, OT_CODE,
							  ORG_ADDR1, ORG_ADDR2, ORG_ADDR3, CT_CODE, PV_CODE, AP_CODE, DT_CODE, ORG_DATE, ORG_JOB,
							  ORG_ID_REF, ORG_ACTIVE, ORG_SEQ_NO, ORG_WEBSITE, UPDATE_USER, UPDATE_DATE, 
							  ORG_ENG_NAME, POS_LAT, POS_LONG, ORG_DOPA_CODE, DEPARTMENT_ID, MG_CODE, PG_CODE
from per_org_ass where org_id =  5;

update per_org_ass set department_id = 3196 where department_id =  5;

update per_personal set org_id = 3196 where org_id =  5;

update per_org_job set org_id = 3196 where org_id =  5;

delete per_org_ass where org_id =  5;
